import React from "react";
import "./roomName.css";

const TitleRoomName = (props) => {
  const rooNameTitle = props.title;
  return (
    <div className="roomName">
        <div className="roomName-box">{rooNameTitle}</div>
      </div>
  );
};

export default TitleRoomName;