import React, { useState } from "react";
import "./roomList.css";
import { Link } from "react-router-dom";

export default function RoomList() {
  const [posts, setPosts] = useState([
    {
      roomId: "1",
      roomName: "Room One",
      playerOneId: "Bimbim",
      playerOneChoice: "scissors",
      playerOneResult: "lose",
      playerTwoId: "Bambang",
      playerTwoChoice: "rock",
      playerTwoResult: "win",
      roomStatus: "Complete",
      winner: "Bambang",
    },
    {
      roomId: "2",
      roomName: "Room Two",
      playerOneId: "Cici",
      playerOneChoice: "rock",
      playerOneResult: "draw",
      playerTwoId: "Coco",
      playerTwoChoice: "rock",
      playerTwoResult: "draw",
      roomStatus: "Complete",
      winner: "Nil (draw)",
    },
    {
      roomId: "3",
      roomName: "Room Three",
      playerOneId: "Dodo",
      playerOneChoice: "paper",
      playerOneResult: "win",
      playerTwoId: "Dede",
      playerTwoChoice: "rock",
      playerTwoResult: "lose",
      roomStatus: "Complete",
      winner: "Dodo",
    },
    {
      roomId: "4",
      roomName: "Room Four",
      playerOneId: "Lili",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "5",
      roomName: "Room Five",
      playerOneId: "Tata",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "6",
      roomName: "Room Six",
      playerOneId: "Meme",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "7",
      roomName: "Room Seven",
      playerOneId: "Popo",
      playerOneChoice: "paper",
      playerOneResult: "lose",
      playerTwoId: "Pepe",
      playerTwoChoice: "scissors",
      playerTwoResult: "win",
      roomStatus: "complete",
      winner: "Pepe",
    },
  ]);
  return (
    <div className="containerRoomList">
      {posts.map((post) => {
        return (
          <Link to={`/dashboard/${post.roomStatus}/${post.roomId}`} style={{textDecoration: "none"}}>
          <div className="singleRoomList">
            <div className="list-roomName">{post.roomName}</div>
            <div>Winner: {post.winner} </div>
            <div>Status: {post.roomStatus} </div>
          </div>
          </Link>
        );
      })}
    </div>
  );
}
