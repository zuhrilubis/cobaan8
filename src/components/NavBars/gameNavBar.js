import React, { useState } from "react";
import "./navBar.css";
import { Link } from "react-router-dom";
import { useFormik } from "formik";

const GameNavBar = () => {
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
  });

  const [user, setUser] = useState({
    username: formik.values.username,
    password: formik.values.password,
  });

  const handleLogout = () => {
    setUser({ ...user, username: "", password: "" });
    localStorage.removeItem("accessToken");
  };

  return (
    <div className="navbarGame">
      <div className="gameLeftBar">
        <Link to="/" style={{ textDecoration: "none" }}>
          <div className="gameHomeBar">HOME</div>
        </Link>
        <Link
          to="/dashboard"
          style={{
            textDecoration: "none",
          }}
        >
          <div className="gameDashboardBar">DASHBOARD</div>
        </Link>
      </div>

      <div className="gameRightBar">
        <Link to="/" style={{ textDecoration: "none" }}>
          <div className="gameLogoutBar" onClick={handleLogout} >LOGOUT</div>
        </Link>
      </div>
    </div>
  );
};

export default GameNavBar;
