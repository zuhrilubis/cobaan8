import React from "react";
import "./navBar.css";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import { Link } from "react-router-dom";

const HomeNavBar = () => {
  return (
    <div className="navbarHome">
      
      <div className="homeLeftBar">
        <Link to="/" style={{ textDecoration: "none" }}>
          <LogoTulisan />
        </Link>
      </div>

      <div className="homeMiddleBar">
        <Link to="/dashboard" style={{ textDecoration: "none" }}>
          <div className="homeDashboardBar">DASHBOARD</div>
        </Link>
        <div className="workBar">WORK</div>
        <div className="contactBar">CONTACT</div>
        <div className="aboutMeBar">ABOUT ME</div>
      </div>

      <div className="homeRightBar">
        <Link to="/daftar" style={{ textDecoration: "none" }}>
          <div className="homeSignupBar">SIGN UP</div>
        </Link>
        <Link to="/login" style={{ textDecoration: "none" }}>
          <div className="homeLoginBar">LOGIN</div>
        </Link>
      </div>
      
    </div>
  );
};

export default HomeNavBar;
