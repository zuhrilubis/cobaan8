import { Link, useLocation } from "react-router-dom";

export default function Nav() {
  const location = useLocation();
  let navTitle;
  let navContent;
  if (location.pathname === "/" || location.pathname === "/login") {
    navTitle = (
      <Link to={"/"} className="logo navbar-brand mx-5 px-4">
        logo
      </Link>
    );
    navContent = (
      <Link to="/register" className="nav-link active">
        sign up
      </Link>
    );
  } else if (location.pathname === "/register") {
    navTitle = (
      <Link to={"/"} className="logo navbar-brand mx-5 px-4">
        logo
      </Link>
    );
    navContent = (
      <Link to="/login" className="nav-link active">
        sign in
      </Link>
    );
  } else {
    navTitle = (
      <Link to={"/dashboard"} className="logo navbar-brand mx-5 px-4">
        dashboard
      </Link>
    );
    navContent = (
      <Link to="/logout" className="nav-link active">
        logout
      </Link>
    );
  }
  return (
    <div>
      <nav className="navbar navbar-dark navbar-expand-lg fw-bold text-uppercase">
        <div className="container-fluid">
          {navTitle}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-5 ps-4">
              <li className="nav-item mx-4">
                <Link to={"/"} className="nav-link active" aria-current="page">
                  home
                </Link>
              </li>
              <li className="nav-item mx-4">
                <Link to={"/"} className="nav-link">
                  work
                </Link>
              </li>
              <li className="nav-item mx-4">
                <Link to={"/"} className="nav-link text-uppercase">
                  contact
                </Link>
              </li>
              <li className="nav-item mx-4">
                <Link to={"/"} className="nav-link">
                  about me
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav me-5 pe-5">
              <li className="nav-item mx-2">{navContent}</li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
