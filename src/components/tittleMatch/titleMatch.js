import React from "react";
import "./titleMatch.css";

const TitleMatch = (props) => {
  const leftTitle = props.leftTitle;
  const rightTitle = props.rightTitle;
  return (
    <div className="player1Vsplayer2-content-tittle">
      <div className="player1Vsplayer2-left-title">{leftTitle}</div>
      <div className="player1Vsplayer2-middleSpace"></div>
      <div className="player1Vsplayer2-right-title">{rightTitle}</div>
    </div>
  );
};

export default TitleMatch;
