import React from "react";
import "./input.css";

const Input = (props) => {
  const className = props.className ?? "input";
  const typeInput = props.type ?? "text";
  const placeholderInput = props.placeholder ?? "fill me please!";
  const name = props.name ?? "name";
  const value = props.value ?? "value";
  const onChange = props.onChange ?? "onChange";
  const onBlur = props.onBlur ?? "onBlur";
  const checked = props.checked ?? "checked";

  return (
    <input
      type={typeInput}
      placeholder={placeholderInput}
      className={className}
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      checked={checked}
    />
  );
};

export default Input;
