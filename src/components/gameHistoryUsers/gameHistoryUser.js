import React, { useState } from "react";
import "./gameHistoryUser.css";

export default function GameHistoryUser() {
  const [posts, setPosts] = useState([
    {
      roomName: "ROOM ONE",
      gameDate: "12 Mei 2023 01.30",
      result: "WIN",
    },
    {
      roomName: "ROOM TWO",
      gameDate: "12 Mei 2023 01.30",
      result: "LOSE",
    },
    {
      roomName: "ROOM THREE",
      gameDate: "12 Mei 2023 01.30",
      result: "DRAW",
    },
    {
      roomName: "VS COM",
      gameDate: "12 Mei 2023 01.30",
      result: "WIN",
    },
    {
      roomName: "VS COM",
      gameDate: "12 Mei 2023 01.30",
      result: "WIN",
    },
  ]);
  return (
    <div className="container-gameHistoryUser">
      {posts.map((post) => {
        return (
          <div className="singleHistoryTable">
            <div className="col-1">{post.roomName}</div>
            <div className="col-2">{post.gameDate}</div>
            <div className="col-3">{post.result}</div>
          </div>
        );
      })}
    </div>
  );
}
