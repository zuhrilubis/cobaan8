import React from "react";
import "./button.css";

const Button = (props) => {
  let buttonTitle = props.title ?? "Klik Me!";

  return <div className="buttonComponent" type={props.type} >{buttonTitle}</div>;
};

export default Button;
