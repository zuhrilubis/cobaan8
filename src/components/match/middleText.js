import React from "react";
import "./middleText.css";

const MiddleText = (props) => {
  let middleTitle = props.title ?? "VS";
   
  return <div className="player1Vsplayer2-content-middle">{middleTitle}</div>;
};

export default MiddleText;