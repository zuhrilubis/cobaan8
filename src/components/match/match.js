import React, { useState } from "react";
import "./match.css";

const Match = () => {
  const [pilihan, setpilihan] = useState("");
  const [middle, setmiddle] = useState("");
  const [border, setborder] = useState("");
  return (
    <div className="player1Vsplayer2-content-match">
      <div className="player1Vsplayer2-content-left">
        <div className="player1Choices">
          <div>
            <img
              className="player1Vsplayer2-rock-left"
              src={require("../../Elements/images/batu.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "rock" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("rock");
                }
                if (middle === "") {
                  setmiddle("green");
                }
                if (border === "") {
                  setborder("none");
                }
              }}
            />
          </div>
          <div>
            <img
              className="player1Vsplayer2-paper-left"
              src={require("../../Elements/images/kertas.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "paper" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("paper");
                }
                if (middle === "") {
                  setmiddle("green");
                }
                if (border === "") {
                  setborder("none");
                }
              }}
            />
          </div>
          <div>
            <img
              className="player1Vsplayer2-scissors-left"
              src={require("../../Elements/images/gunting.png")}
              alt=""
              style={{
                backgroundColor: pilihan === "scissors" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("scissors");
                }
                if (middle === "") {
                  setmiddle("green");
                }
                if (border === "") {
                  setborder("none");
                }
              }}
            />
          </div>
        </div>
      </div>
      <div
        className="player1Vsplayer2-content-middle"
        style={{
          backgroundColor: middle,
          border: border,
        }}
      >
        VS
      </div>
      <div className="player1Vsplayer2-content-right">
        <div className="comChoices">
          <div>
            <img
              className="player1Vsplayer2-rock-right"
              src={require("../../Elements/images/batu.png")}
              alt=""
            />
          </div>
          <div>
            <img
              className="player1Vsplayer2-paper-right"
              src={require("../../Elements/images/kertas.png")}
              alt=""
            />
          </div>
          <div>
            <img
              className="player1Vsplayer2-paper-right"
              src={require("../../Elements/images/gunting.png")}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Match;
