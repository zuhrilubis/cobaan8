import { Route, Routes } from "react-router-dom";
import "./App.css";
import LoginPage from "./pages/Logins/login";
import RegistrationPage from "./pages/Registrations/registration";
import DashboardPage from "./pages/Dashboards/dashboard";
import CreateRoomPage from "./pages/createrooms/createRoom";
import HomePage from "./pages/Homes/home";
import PlayerVsComPage from "./pages/PlayerVSComs/playerVsCom";
import AvailableRoomPage from "./pages/SingleRooms/availableRoom";
import GameHistyoryPage from "./pages/GameHistories/gameHistory";
import CompleteRoomPage from "./pages/SingleRooms/completeRoom";
import UpdateBioPage from "./pages/Biodata/updateBio";
import CreatedRoomPage from "./pages/SingleRooms/createdRoom";
import Authentication from "./Elements/authentication";
import Base from "./pages/LearnFirebase/base";

function App() {
  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/daftar" element={<RegistrationPage />} />
        <Route path="/dashboard">
          <Route path="" element={<Authentication><DashboardPage /> </Authentication>} />
          <Route
            path="/dashboard/available/:roomId"
            element={<AvailableRoomPage />}
          />
          <Route
            path="/dashboard/complete/:roomId"
            element={<CompleteRoomPage />}
          />
        </Route>
        <Route path="/createroom">
          <Route path="" element={<CreateRoomPage />} />
          <Route path="/createroom/createdroom" element={<CreatedRoomPage />} />
        </Route>
        <Route path="/playerVScom" element={<PlayerVsComPage />} />
        <Route path="/gamehistory" element={<GameHistyoryPage />} />
        <Route path="/updatebio" element={<UpdateBioPage />} />
        <Route path="*" element={<div>Halaman 404</div>} />

        <Route path="/firebase" element={<Base/>} />

      </Routes>
    </div>
  );
}

export default App;
