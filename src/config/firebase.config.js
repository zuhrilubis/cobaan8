import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyD9L1yq8FW2TOH0vgaVW1Ul7kxJuDrLBAY",
  authDomain: "zuhri-binar31.firebaseapp.com",
  projectId: "zuhri-binar31",
  storageBucket: "zuhri-binar31.appspot.com",
  messagingSenderId: "97497160056",
  appId: "1:97497160056:web:4f9159add2c3f83088d08d"
};

const app = initializeApp(firebaseConfig);

export const storage = getStorage(app);