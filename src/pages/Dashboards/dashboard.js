import React, { useEffect, useState } from "react";
import "./dashboard.css";
import RoomList from "../../components/roomlists/roomList";
import GameNavBar from "../../components/NavBars/gameNavBar";
import { Link } from "react-router-dom";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "../../config/firebase.config";

const DashboardPage = () => {
  const [dataBio, setdataBio] = useState("");

  const tampilanDataBio = async () => {
    const accessTokenYangTersimpan = localStorage.getItem("accessToken");
    const accessToken = "Bearer " + accessTokenYangTersimpan;
    try {
      const getData = await axios
        .get("https://be-binar-zuhri.vercel.app/users/userIdBio", {
          headers: { Authorization: `${accessToken}` },
        })
        .then((response) => {
          return response.data;
        });

      setdataBio(getData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    tampilanDataBio();
  }, []);

  console.log(dataBio.userName);

  const [popup, setPop] = useState(false);
  const handleClickOpen = () => {
    setPop(!popup);
  };
  const closePopup = () => {
    setPop(false);
  };

  const token = localStorage.getItem("accessToken");
  const decode = jwtDecode(token);
  console.log(decode.id);

  const [fileMauDiUpload, setFileMauDiUpload] = useState(null);
  const [assetDiFirebaseStorage, setAssetDiFirebaseStorage] = useState(null);

  const handleClickHapusPict = () => {
    setAssetDiFirebaseStorage ("https://firebasestorage.googleapis.com/v0/b/zuhri-binar31.appspot.com/o/UserId-29%2F2023-07-01T15%3A10%3A49.405Z-Ali%20hasan.JPG?alt=media&token=daed4f72-fb6d-4ff8-a448-db6770b975f3")
  }

  return (
    <div className="dashboardContainer">
      <GameNavBar />
      {popup ? (
        <div className="popup-profilPict-container">
          <div className="close-popup" onClick={closePopup}>X</div>
          <div className="popup-header"> Change profil picture</div>
          <input
            className="inputComponent"
            type="file"
            onChange={(e) => {
              setFileMauDiUpload(e.target.files[0]);
            }}
          />
          <div className="hapus-profilPict" onClick={handleClickHapusPict}>Hapus profil picture</div>
          <button
            className="buttonComponent"
            onClick={async () => {
              // generate date
              const date = new Date();
              const assetPath = `UserId-${decode.id}/${date.toISOString()}-${
                fileMauDiUpload.name
              }`;
              const imageRef = ref(storage, assetPath);
              const imageUploaded = await uploadBytes(
                imageRef,
                fileMauDiUpload
              );
              // simpan asset path di dalam database
              // setAssetDiFirebaseStorage(assetPath);

              getDownloadURL(imageRef).then((res) =>
                setAssetDiFirebaseStorage(res)
              );
              
            }}
          >
            Submit
          </button>
        </div>
      ) : (
        ""
      )}
      <div className="dash-containerBelow">
        <div className="leftBelow">
          <div className="playVSCom">
            <Link to="/playerVScom" style={{ textDecoration: "none" }}>
              <div className="playVSComBox">PLAY VS COM</div>
            </Link>
          </div>
          <div className="singleLine">
            <hr></hr>
          </div>
          <div className="createNewRoom">
            <Link to="/createroom" style={{ textDecoration: "none" }}>
              <div className="createNewRoomText">Create New Room</div>
            </Link>
          </div>
          <div className="roomList">
            <RoomList />
          </div>
        </div>
        <div className="rightBelow">
          <div className="userPict">
            <img
              className="userPict-box"
              src={
                assetDiFirebaseStorage
                  ? assetDiFirebaseStorage
                  : ""
              }
              alt="profil picture"
              onClick={handleClickOpen}
            />
            <div className="userPict-name">{dataBio?.userName}</div>
          </div>
          <div className="userBio">
            <div className="userBio-raw">
              <div className="userBio-raw-title">Fullname</div>
              <div className="userBio-raw-colon">:</div>
              <div className="userBio-raw-value">
                {dataBio?.UserBio?.fullName}
              </div>
            </div>
            <div className="userBio-raw">
              <div className="userBio-raw-title">Phone Number</div>
              <div className="userBio-raw-colon">:</div>
              <div className="userBio-raw-value">
                {dataBio?.UserBio?.phoneNumber}
              </div>
            </div>
            <div className="userBio-raw">
              <div className="userBio-raw-title">Address</div>
              <div className="userBio-raw-colon">:</div>
              <div className="userBio-raw-value">
                {dataBio?.UserBio?.address}
              </div>
            </div>
          </div>
          <div className="updateBio">
            <div>
              Update your Biodata{" "}
              <Link
                to="/updatebio"
                style={{ textDecoration: "none", fontWeight: "bold" }}
              >
                <span>here</span>
              </Link>
            </div>
          </div>
          <Link to="/gameHistory" style={{ textDecoration: "none" }}>
            <div className="gameHistory"> Game History </div>
          </Link>
          <Link to="/firebase" style={{ textDecoration: "none" }}>
            <div className="gameHistory"> Try Firebase </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default DashboardPage;
