import React, { useState } from "react";
import "./updateBio.css";
import "../Homes/home.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
// import { updateBioSchema } from "../../Elements/validationSchema";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import axios from "axios";

const UpdateBioPage = () => {
  const navigate = useNavigate();
  const [errorMessage, seterrorMessage] = useState("");
  const [user, setUser] = useState({
    fullname: "",
    phonenumber: "",
    address: "",
  });

  const accessTokenYangTersimpan = localStorage.getItem("accessToken");
  const accessToken = "Bearer " + accessTokenYangTersimpan;

  const handleUpdateBio = async () => {
    setUser({
      ...user,
      fullname: formik.values.fullname,
      phonenumber: formik.values.phonenumber,
      address: formik.values.address,
    });
    try {
      const isianDataBio = await axios
        .put(
          "https://be-binar-zuhri.vercel.app/users/updateBio",
          {
            fullName: formik.values.fullname,
            phoneNumber: formik.values.phonenumber,
            address: formik.values.address,
          },
          {
            headers: { Authorization: `${accessToken}` },
          }
        )
        .then((response) => {
          return response.data.message;
        });
      console.log(isianDataBio);
      alert("Success, your biodata is being update");
      navigate("/dashboard");
    } catch (error) {
      console.log(error);
      alert("Oops..Update biodata failed");
      const errorMessage = error.response.data.messages;
      seterrorMessage(errorMessage);
    }
  };

  const formik = useFormik({
    initialValues: {
      fullname: "",
      phonenumber: "",
      address: "",
    },

    onSubmit: handleUpdateBio,
  });

  console.log(formik);

  return (
    <div className="pageUpdateBio">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="updateBio-midle-container">
        <div className="coverUpdateBio">
          <div className="updateBioLogo">
            <Link to="/" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleUpdateBio">
            Update your biodata, enjoy it!
          </div>
          <form onSubmit={formik.handleSubmit}>
            <input
              className="inputComponent"
              type="text"
              placeholder="Full Name"
              name="fullname"
              {...formik.getFieldProps("fullname")}
              required="true"
              minLength={7}
            />
            {/* <div className="errorInputUpdateBio">
              {formik.touched.fullname && formik.errors.fullname && (
                <div className="updateBioError">
                  {formik.errors.fullname}{" "}
                </div>
              )}
            </div> */}

            <input
              className="inputComponent"
              type="integer"
              placeholder="Phone Number"
              name="phonenumber"
              {...formik.getFieldProps("phonenumber")}
              required="true"
              minLength={10}
            />
            {/* <div className="errorInputUpdateBio">
              {formik.touched.phonenumber && formik.errors.phonenumber && (
                <div className="updateBioError">
                  {formik.errors.phonenumber}{" "}
                </div>
              )}
            </div> */}

            <input
              className="inputComponent"
              type="text"
              placeholder="Address"
              name="address"
              {...formik.getFieldProps("address")}
              required="true"
            />
            {/* <div className="errorInputUpdateBio">
              {formik.touched.address && formik.errors.address && (
                <div className="updateBioError">
                  {formik.errors.address}{" "}
                </div>
              )}
            </div> */}

            <div className="buttonUpdateBio">
              <button className="buttonComponent" type="submit">
                UPDATE
              </button>
            </div>
          </form>
          <div className="errorInputUpdateBio">{errorMessage}</div>
        </div>
      </div>
    </div>
  );
};

export default UpdateBioPage;
