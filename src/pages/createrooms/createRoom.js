import React, { useState } from "react";
import "./createRoom.css";
import "../../components/inputs/input.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";

const CreateRoomPage = () => {
  const navigate = useNavigate();
  const [roomName, setroomName] = useState("");
  const [pilihan, setpilihan] = useState("");

  const handleSave = () => {
    setroomName(formik.values.roomName);
    
    const accessTokenroomName = formik.values.roomName;
    localStorage.setItem("accessTokenRoomName", accessTokenroomName);
    
    navigate("/createroom/createdroom");
  };

  const formik = useFormik({
    initialValues: {
      roomName: "",
    },
    // validationSchema: registrationSchema,

    onSubmit: handleSave,
  });

  return (
    <div className="createRoomContainer">
      <GameNavBar />
      <div className="creatRoomContent">
        <form onSubmit={formik.handleSubmit}>
          <div className="labelCreateRoom">Create Your Room Here!</div>
          <div className="createRoomInput">
            <input
              className="inputComponent"
              type="text"
              placeholder="Input Your Game Room Name"
              name="roomName"
              {...formik.getFieldProps("roomName")}
              required="true"
              minLength={4}
            />
          </div>
          <div className="yourChoice">Your Choice Please!</div>
          <div className="choices">
            <img
              className="rock"
              src={require("../../Elements/images/batu.png")}
              alt="batu"
              style={{
                backgroundColor: pilihan === "rock" ? "lightpink" : "",
              }}
              onClick={() => {
                if (pilihan === "") {
                  setpilihan("rock");
                  localStorage.setItem("accessTokenPilihan", ("rock"));
                }
              }}
            />
            <img
              className="paper"
              src={require("../../Elements/images/kertas.png")}
              alt="kertas"
              style={{
                backgroundColor: pilihan === "paper" ? "lightpink" : "",
              }}
              onClick={(e) => {
                if (pilihan === "") {
                  setpilihan("paper");
                  localStorage.setItem("accessTokenPilihan", ("paper"));
                }
              }}
            />
            <img
              className="paper"
              src={require("../../Elements/images/gunting.png")}
              alt="gunting"
              style={{
                backgroundColor: pilihan === "scissors" ? "lightpink" : "",
              }}
              onClick={(e) => {
                if (pilihan === "") {
                  setpilihan("scissors");
                  localStorage.setItem("accessTokenPilihan", ("scissors"));
                }
              }}
            />
          </div>
          <div className="buttonCreateRoom">
            <button className="buttonComponent" type="submit">
              SAVE
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CreateRoomPage;
