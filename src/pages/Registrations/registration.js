import React, { useState } from "react";
import "./registration.css";
import "../Homes/home.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import { useFormik } from "formik";
import axios from "axios";
import { registrationSchema } from "../../Elements/validationSchema";
import Input from "../../components/inputs/input";

const RegistrationPage = (props) => {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [errorMessage, seterrorMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleRegister = async () => {
    setUser({
      ...user,
      username: formik.values.username,
      email: formik.values.email,
      password: formik.values.password,
    });

    try {
      const submittedData = await axios.post(
        "https://be-binar-zuhri.vercel.app/users/daftar",
        {
          userName: formik.values.username,
          email: formik.values.email,
          password: formik.values.password,
        }
      );
      alert("Registration succeed! New user is being added");
      console.log(submittedData.data);
      navigate("/login");
    } catch (error) {
      alert("Oops..registration failed");
      const errorMessage = error.response.data.message;
      seterrorMessage(errorMessage);
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      password: "",
    },
    validationSchema: registrationSchema,
    onSubmit: handleRegister,
  });

  console.log(formik);

  return (
    <div className="pageRegistration">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="registration-midle-container">
        <div className="coverRegistration">
          <div className="registrationLogo">
            <Link to="/" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleRegistration">
            Create your account to get more pleasure, enjoy it!
          </div>
          <div className="errorInputRegistration">{errorMessage}</div>
          <form onSubmit={formik.handleSubmit}>
            <Input
              className="inputComponent"
              type="text"
              placeholder="Username"
              name="username"
              {...formik.getFieldProps("username")}
            />
            <div className="errorInput">
              {formik.touched.username && formik.errors.username && (
                <div className="registrationError">
                  {formik.errors.username}{" "}
                </div>
              )}
            </div>

            <Input
              className="inputComponent"
              type="email"
              placeholder="Email"
              name="email"
              {...formik.getFieldProps("email")}
            />
            <div className="errorInput">
              {formik.touched.email && formik.errors.email && (
                <div className="registrationError">{formik.errors.email} </div>
              )}
            </div>

            <div className="password-input-wrapper">
              <Input
                className="inputComponent"
                type={showPassword ? "text" : "password"}
                placeholder="Password"
                name="password"
                {...formik.getFieldProps("password")}
              />
              <div className="errorInput">
                {formik.touched.password && formik.errors.password && (
                  <div className="registrationError">
                    {formik.errors.password}{" "}
                  </div>
                )}
              </div>
              <button
                type="button"
                className="password-toggle-button"
                onClick={() => setShowPassword((prevState) => !prevState)}
              >
                {showPassword ? (
                  <div className="eye-close"></div>
                ) : (
                  <div className="eye-open"></div>
                )}
              </button>
            </div>

            <div className="buttonRegistration">
              <button className="buttonComponent" type="submit">
                SIGN UP
              </button>
            </div>
          </form>
          <div className="textBelow-registration">
            Already have an account?{" "}
            <span className="textBelow-registration-link">
              <Link to="/login" style={{ textDecoration: "none"}}>
                Sign In here
              </Link>
            </span>{" "}
            please!
          </div>
          
        </div>
      </div>
    </div>
  );
};

export default RegistrationPage;
