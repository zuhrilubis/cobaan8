import React, { useEffect, useState } from "react";
import "./completeRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import TitleMatch from "../../components/tittleMatch/titleMatch";
import { useParams } from "react-router-dom";

const CompleteRoomPage = () => {
  const roomDetail = [
    {
      roomId: "1",
      roomName: "Room One",
      playerOneId: "Bimbim",
      playerOneChoice: "scissors",
      playerOneResult: "lose",
      playerTwoId: "Bambang",
      playerTwoChoice: "rock",
      playerTwoResult: "win",
      roomStatus: "Complete",
      winner: "Username 2",
    },
    {
      roomId: "2",
      roomName: "Room Two",
      playerOneId: "Cici",
      playerOneChoice: "rock",
      playerOneResult: "draw",
      playerTwoId: "Coco",
      playerTwoChoice: "rock",
      playerTwoResult: "draw",
      roomStatus: "Complete",
      winner: "Nil (draw)",
    },
    {
      roomId: "3",
      roomName: "Room Three",
      playerOneId: "Dodo",
      playerOneChoice: "paper",
      playerOneResult: "win",
      playerTwoId: "Dede",
      playerTwoChoice: "rock",
      playerTwoResult: "lose",
      roomStatus: "Complete",
      winner: "Username 1",
    },
    {
      roomId: "4",
      roomName: "Room Four",
      playerOneId: "Lili",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "5",
      roomName: "Room Five",
      playerOneId: "Tata",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "6",
      roomName: "Room Six",
      playerOneId: "Meme",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "7",
      roomName: "Room Seven",
      playerOneId: "Popo",
      playerOneChoice: "paper",
      playerOneResult: "lose",
      playerTwoId: "Pepe",
      playerTwoChoice: "scissors",
      playerTwoResult: "win",
      roomStatus: "complete",
      winner: "Pepe",
    },
  ];

  const { roomId } = useParams();
  const roomChoosed = roomDetail[roomId - 1];
  const [result, setresult] = useState("")
 

  useEffect(() => {
    if (roomChoosed.playerOneChoice === "rock") {
      switch (roomChoosed.playerTwoChoice) {
        case "paper":
          return setresult("PLAYER 2 WIN");
        case "scissors":
          return setresult("PLAYER 1 WIN");
        case "rock":
          return setresult("DRAW");
      }
    } else if (roomChoosed.playerOneChoice === "paper") {
      switch (roomChoosed.playerTwoChoice) {
        case "rock":
          return setresult("PLAYER 1 WIN");
        case "scissors":
          return setresult("PLAYER 2 WIN");
        case "paper":
          return setresult("DRAW");
      }
    } else if (roomChoosed.playerOneChoice === "scissors") {
      switch (roomChoosed.playerTwoChoice) {
        case "rock":
          return setresult("PLAYER 2 WIN");
        case "paper":
          return setresult("PLAYER 1 WIN");
        case "scissors":
          return setresult("DRAW");
      }
    }
  },[roomChoosed.playerOneChoice, roomChoosed.playerTwoChoice]);

  return (
    <div className="completeRoom-Container">
      <GameNavBar />

      <div className="completeRoom-name">
        <div className="completeRoom-name-box">{roomChoosed.roomName}</div>
      </div>
      <TitleMatch leftTitle={roomChoosed.playerOneId} rightTitle={roomChoosed.playerTwoId} />
      <div className="player1Vsplayer2-content-matchComplete">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftComplete"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerOneChoice === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftComplete"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerOneChoice === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftComplete"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerOneChoice === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleComplete"
          style={{
            backgroundColor: "green",
            border: "none",
            fontWeight: "bold",
          }}
        >
          {result === "" ? "VS" : `${result}`}
        </div>
        <div className="player1Vsplayer2-content-right">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightComplete"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerTwoChoice === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightComplete"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerTwoChoice === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightComplete"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: roomChoosed.playerTwoChoice === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompleteRoomPage;
