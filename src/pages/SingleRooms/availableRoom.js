import React, { useEffect, useState } from "react";
import "./availableRoom.css";
import GameNavBar from "../../components/NavBars/gameNavBar";
import TitleMatch from "../../components/tittleMatch/titleMatch";
import { useParams } from "react-router-dom";

const AvailableRoomPage = () => {
  const roomDetail = [
    {
      roomId: "1",
      roomName: "Room One",
      playerOneId: "Bimbim",
      playerOneChoice: "scissors",
      playerOneResult: "lose",
      playerTwoId: "Bambang",
      playerTwoChoice: "rock",
      playerTwoResult: "win",
      roomStatus: "Complete",
      winner: "Username 2",
    },
    {
      roomId: "2",
      roomName: "Room Two",
      playerOneId: "Cici",
      playerOneChoice: "rock",
      playerOneResult: "draw",
      playerTwoId: "Coco",
      playerTwoChoice: "rock",
      playerTwoResult: "draw",
      roomStatus: "Complete",
      winner: "Nil (draw)",
    },
    {
      roomId: "3",
      roomName: "Room Three",
      playerOneId: "Dodo",
      playerOneChoice: "paper",
      playerOneResult: "win",
      playerTwoId: "Dede",
      playerTwoChoice: "rock",
      playerTwoResult: "lose",
      roomStatus: "Complete",
      winner: "Username 1",
    },
    {
      roomId: "4",
      roomName: "Room Four",
      playerOneId: "Lili",
      playerOneChoice: "paper",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "5",
      roomName: "Room Five",
      playerOneId: "Tata",
      playerOneChoice: "rock",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "6",
      roomName: "Room Six",
      playerOneId: "Meme",
      playerOneChoice: "scissors",
      playerOneResult: "waiting player 2.....",
      playerTwoId: "waiting player 2.....",
      playerTwoChoice: "",
      playerTwoResult: "",
      roomStatus: "available",
      winner: "..........",
    },
    {
      roomId: "7",
      roomName: "Room Seven",
      playerOneId: "Popo",
      playerOneChoice: "kertas",
      playerOneResult: "lose",
      playerTwoId: "Username 2",
      playerTwoChoice: "scissors",
      playerTwoResult: "win",
      roomStatus: "complete",
      winner: "Username 2",
    },
  ];

  const { roomId } = useParams();
  const roomChoosed = roomDetail[roomId - 1];
  const accessToken = localStorage.getItem("accessTokenLog");

  const [pilihan1, setpilihan1] = useState("");
  const [pilihan2, setpilihan2] = useState("");
  const [middle, setmiddle] = useState("");
  const [border, setborder] = useState("");
  const [result, setresult] = useState("")
  const [fontweight, setfontweight] = useState("")

  useEffect(() => {
    if (pilihan1 === "rock") {
      switch (pilihan2) {
        case "paper":
          return setresult("YOU WIN");
        case "scissors":
          return setresult("YOU LOSE");
        case "rock":
          return setresult("DRAW");
      }
    } else if (pilihan1 === "paper") {
      switch (pilihan2) {
        case "rock":
          return setresult("YOU LOSE");
        case "scissors":
          return setresult("YOU WIN");
        case "paper":
          return setresult("DRAW");
      }
    } else if (pilihan1 === "scissors") {
      switch (pilihan2) {
        case "rock":
          return setresult("YOU WIN");
        case "paper":
          return setresult("YOU LOSE");
        case "scissors":
          return setresult("DRAW");
      }
    }
  },[pilihan1, pilihan2]);

  return (
    <div className="availableRoom-Container">
      <GameNavBar />

      <div className="availableRoom-name">
        <div className="availableRoom-name-box">{roomChoosed.roomName}</div>
      </div>
      <TitleMatch
        leftTitle={roomChoosed.playerOneId}
        rightTitle={accessToken}
      />
      <div className="player1Vsplayer2-content-matchAvailable">
        <div className="player1Vsplayer2-content-left">
          <div className="player1Choices">
            <div>
              <img
                className="player1Vsplayer2-rock-leftAvailable"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "rock" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-leftAvailable"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "paper" ? "lightpink" : "",
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-scissors-leftAvailable"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan1 === "scissors" ? "lightpink" : "",
                }}
              />
            </div>
          </div>
        </div>
        <div
          className="player1Vsplayer2-content-middleAvailable"
          style={{
            backgroundColor: middle,
            border: border,
            fontWeight: fontweight,
          }}
        >
          {result === "" ? "VS" : `${result}`}
        </div>
        <div className="player1Vsplayer2-content-right">
          <div className="comChoices">
            <div>
              <img
                className="player1Vsplayer2-rock-rightAvailable"
                src={require("../../Elements/images/batu.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "rock" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("rock");
                    setpilihan1(roomChoosed.playerOneChoice);
                    setfontweight("bold");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightAvailable"
                src={require("../../Elements/images/kertas.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "paper" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("paper");
                    setpilihan1(roomChoosed.playerOneChoice);
                    setfontweight("bold");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
            <div>
              <img
                className="player1Vsplayer2-paper-rightAvailable"
                src={require("../../Elements/images/gunting.png")}
                alt=""
                style={{
                  backgroundColor: pilihan2 === "scissors" ? "lightpink" : "",
                }}
                onClick={() => {
                  if (pilihan2 === "") {
                    setpilihan2("scissors");
                    setpilihan1(roomChoosed.playerOneChoice);
                    setfontweight("bold");
                  }
                  if (middle === "") {
                    setmiddle("green");
                  }
                  if (border === "") {
                    setborder("none");
                  }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AvailableRoomPage;
