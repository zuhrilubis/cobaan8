import React, { useState } from "react";
import "./login.css";
import HomeNavBar from "../../components/NavBars/homeNavBar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { loginSchema } from "../../Elements/validationSchema";
import LogoTulisan from "../../Elements/logo-tulisan/logoTulisan";
import axios from "axios";
import Input from "../../components/inputs/input";

const LoginPage = () => {
  const navigate = useNavigate();

  const [user, setUser] = useState({ username: "", password: "" });
  const [errorMessage, seterrorMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleLogin = async () => {
    setUser({
      ...user,
      username: formik.values.username,
      password: formik.values.password,
    });
    try {
      const loggingData = await axios.post(
        "https://be-binar-zuhri.vercel.app/users/login",
        { userName: formik.values.username, password: formik.values.password }
      );
      const token = loggingData.data.accessToken;
      localStorage.setItem("accessToken", token);
      alert("Success, you are logged in");
      navigate("/dashboard");
    } catch (error) {
      console.log(error);
      alert("Oops..login failed");
      const errorMessage = error.response.data.message;
      seterrorMessage(errorMessage);
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: handleLogin,
  });

  console.log(formik);

  return (
    <div className="pageLogin">
      <div className="homeNavBar">
        <HomeNavBar />
      </div>
      <div className="login-midle-container">
        <div className="coverLogin">
          <div className="loginLogo">
            <Link to="/" style={{ textDecoration: "none" }}>
              <LogoTulisan />
            </Link>
          </div>
          <div className="subTitleLogin">
            Welcome to the game! <br></br>Before playing the game, Sign in first
            and then, enjoy the game!
          </div>
          <div className="errorInputLogin">{errorMessage}</div>
          <form onSubmit={formik.handleSubmit}>
            {/* Input field for username */}
            <Input
              className="inputComponent"
              type="text"
              placeholder="Username"
              name="username"
              {...formik.getFieldProps("username")}
            />
            <div className="errorInput">
              {formik.touched.username && formik.errors.username && (
                <div className="registrationError">
                  {formik.errors.username}{" "}
                </div>
              )}
            </div>
            {/* Input field for password */}
            <div className="password-input-wrapper">
              <Input
                className="inputComponent"
                type={showPassword ? "text" : "password"}
                placeholder="Password"
                name="password"
                {...formik.getFieldProps("password")}
              />
              <div className="errorInput">
                {formik.touched.password && formik.errors.password && (
                  <div className="registrationError">
                    {formik.errors.password}{" "}
                  </div>
                )}
              </div>

              <button
                type="button"
                className="password-toggle-button"
                onClick={() => setShowPassword((prevState) => !prevState)}
              >
                {showPassword ? (
                  <div className="eye-close"></div>
                ) : (
                  <div className="eye-open"></div>
                )}
              </button>
            </div>
            <div className="buttonLogin">
              <button className="buttonComponent" type="submit">
                SIGN IN
              </button>
            </div>
          </form>
          <div className="textBelow-login">
            Don't have account yet?{" "}
            <span className="textBelow-login-link">
              <Link to="/daftar" style={{ textDecoration: "none" }}>
                Sign Up here
              </Link>
            </span>{" "}
            please!
          </div>
          
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
