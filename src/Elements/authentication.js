import { Navigate } from "react-router-dom";

const Authentication = ({ children }) => {
  const accessToken = localStorage.getItem("accessToken");

  if (!accessToken) {
    return <Navigate to={"/login"} />;
  }
  if (accessToken) {
    return <>{children}</>;
  }
};

export default Authentication;
